export interface IPhoneCodeRecord {
  mask: string;
  cc: string | string[];
  name_en: string;
  desc_en: string;
  name_ru: string;
  desc_ru: string;
}

export type PhoneCodes = IPhoneCodeRecord[];

export interface IPhoneCodesForest {
  record?: IPhoneCodeRecord;
  children: {
    [digit: string]: IPhoneCodesForest;
  };
}
