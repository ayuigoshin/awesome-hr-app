export interface ISelection {
  [tag: string]: boolean | undefined;
}
