export interface IEmployeeCreate {
  name: string;
  location: string;
  birthDate: string;
  phone: string;
  tags: string[];
}

export interface IEmployee extends IEmployeeCreate {
  id: number;
}
