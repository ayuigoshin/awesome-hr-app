export interface IPaginationResult<T> {
  items: T[];
  totalCount: number;
}
