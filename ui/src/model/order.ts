export type OrderBy<T> = keyof T;
export type Order = 'ASC' | 'DESC';
