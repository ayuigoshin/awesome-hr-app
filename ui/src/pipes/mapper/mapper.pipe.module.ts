import {NgModule} from '@angular/core';

import {MapperPipe} from './mapper.pipe';

@NgModule({
  imports: [],
  exports: [MapperPipe],
  declarations: [MapperPipe],
  providers: [],
})
export class MapperPipeModule {
}
