import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'mapper',
})
export class MapperPipe<T, R> implements PipeTransform {
  transform(value: T, mapperFunction: (item: T, ...args: any[]) => R, ...args: any[]): R {
    return mapperFunction(value, args);
  }
}
