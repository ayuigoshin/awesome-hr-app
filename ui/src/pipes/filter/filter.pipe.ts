import {Pipe, PipeTransform} from '@angular/core';
import { isNil } from 'src/util/utils';

const EMPTY: any[] = [];

@Pipe({
  name: 'filter',
})
export class FilterPipe<T> implements PipeTransform {
  transform(items: T[] | null, filterFunction: (item: T, ...args: any[]) => boolean, ...args: any[]): T[] {
    if (isNil(items)) {
      return EMPTY;
    }

    return items.filter(item => filterFunction(item, ...args));
  }
}
