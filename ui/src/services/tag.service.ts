import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {toHttpParams} from '../util/utils';
import {Observable} from 'rxjs';
import {IPaginationResult} from '../model/pagination';

@Injectable()
export class TagService {

  constructor(private readonly http: HttpClient) {
  }

  getAll(query?: string, offset?: number, limit?: number): Observable<IPaginationResult<string>> {
    return this.http.get<IPaginationResult<string>>('/api/v1/tag', {
      params: toHttpParams({
        query,
        offset,
        limit,
      }),
    });
  }

  add(tags: string[]): Observable<void> {
    return this.http.post<void>('/api/v1/tag', tags);
  }

  remove(tag: string): Observable<IPaginationResult<string>> {
    return this.http.delete<IPaginationResult<string>>(`/api/v1/tag/${tag}`);
  }
}
