import {Inject, Injectable} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {isNil} from '../util/utils';
import {EMPTY, from, fromEvent, Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, startWith} from 'rxjs/operators';

@Injectable()
export class AdaptiveService {
  xs$ = this.observeResize(() => this.xs);
  s$ = this.observeResize(() => this.s);
  m$ = this.observeResize(() => this.m);
  l$ = this.observeResize(() => this.l);
  xl$ = this.observeResize(() => this.xl);

  constructor(@Inject(DOCUMENT) private document: Document) {
  }

  /**
   * Returns true if window width < 576px, else false.
   *
   * @return boolean
   */
  get xs(): boolean {
    return this.width < 576;
  }

  /**
   * Returns true if window width ≥ 576px, else false.
   *
   * @return boolean
   */
  get s(): boolean {
    return this.width >= 576;
  }

  /**
   * Returns true if window width ≥ 768px, else false.
   *
   * @return boolean
   */
  get m(): boolean {
    return this.width >= 768;
  }

  /**
   * Returns true if window width ≥ 992px, else false.
   *
   * @return boolean
   */
  get l(): boolean {
    return this.width >= 992;
  }

  /**
   * Returns true if window width ≥ 1200px, else false.
   *
   * @return boolean
   */
  get xl(): boolean {
    return this.width >= 1200;
  }

  private get window(): Window | null {
    return this.document.defaultView;
  }

  private get width(): number {
    return isNil(this.window) ? 1920 : this.window.innerWidth;
  }

  private observeResize<T>(onResize: () => T): Observable<T> {
    if (!this.window) {
      return EMPTY;
    }

    return fromEvent(this.window, 'resize').pipe(
      startWith(onResize()),
      debounceTime(300),
      map(onResize),
      distinctUntilChanged(),
    );
  }
}
