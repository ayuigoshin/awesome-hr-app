import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable()
export class TagsManagementService {
  readonly open$ = new Subject<boolean>();

  open() {
    this.open$.next(true);
  }

  close() {
    this.open$.next(false);
  }
}
