import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {toHttpParams} from '../util/utils';
import {Observable} from 'rxjs';
import {IEmployee, IEmployeeCreate} from '../model/employee';
import {IPaginationResult} from '../model/pagination';
import {IDeleteResponse} from '../model/response';
import {OrderBy, Order} from '../model/order';

@Injectable()
export class EmployeeService {

  constructor(private readonly http: HttpClient) {
  }

  getAll(query?: string,
         offset?: number,
         limit?: number,
         tags?: string[],
         orderBy?: OrderBy<IEmployee>,
         order?: Order): Observable<IPaginationResult<IEmployee>> {
    return this.http.get<IPaginationResult<IEmployee>>('/api/v1/employee', {
      params: toHttpParams({
        query,
        offset,
        limit,
        tags: tags && tags.length ? tags.join(',') : null,
        orderBy,
        order,
      })
    });
  }

  get(id: number): Observable<IEmployee> {
    return this.http.get<IEmployee>(`/api/v1/employee/${id}`);
  }

  create(employee: IEmployeeCreate): Observable<IEmployee> {
    return this.http.post<IEmployee>('/api/v1/employee', employee);
  }

  update(id: number, employee: IEmployee) {
    return this.http.put<IEmployee>(`/api/v1/employee/${id}`, employee);
  }

  patch(id: number, employee: Partial<IEmployee>): Observable<IEmployee> {
    return this.http.patch<IEmployee>(`/api/v1/employee/${id}`, employee);
  }

  delete(id: number): Observable<IDeleteResponse> {
    return this.http.delete<IDeleteResponse>(`/api/v1/employee/${id}`);
  }
}
