import {Injectable} from '@angular/core';
import {IPhoneCodeRecord, IPhoneCodesForest, PhoneCodes} from '../model/phoneCodes';
import {isNil} from '../util/utils';
import codes from '../../data/phone-codes.json';
import {InputMask} from '../model/inputMask';
import {max} from 'rxjs/operators';

const DEFAULT_MASK = ['+', /[1-9]/, ' ', /[1-9]/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/];

@Injectable()
export class PhoneMaskService {
  readonly phoneCodesForest = this.initForest(codes);

  getPhoneMask(rawValue: string) {
    const phone = rawValue.replace(/\D+/g, '');
    const record = this.findRecord(phone, this.phoneCodesForest);

    if (!record) {
      return DEFAULT_MASK;
    }

    return this.getMaskForRecord(record);
  }

  private getMaskForRecord(record: IPhoneCodeRecord): InputMask {
    const {mask} = record;

    return mask.split('').reduce((mask, char) => {
      if (/\d/.test(char) || char === '#') {
        return [...mask, /\d/];
      }

      if (char === '+') {
        return ['+', ...mask];
      }

      return [...mask, ' '];
    }, []);
  }

  private findRecord(phone: string, forest: IPhoneCodesForest, index = 0): IPhoneCodeRecord | null {
    const digit = phone[index];

    if (isNil(digit)) {
      return forest.record || null;
    }

    if (!forest.children[digit]) {
      return forest.record || null;
    }

    return this.findRecord(phone, forest.children[digit], index + 1);
  }

  private initForest(codes: PhoneCodes): IPhoneCodesForest {
    return codes.reduce((forest, record) => {
        const code = record.mask.replace(/#.*/g, '')
          .replace(/\D+/g, '');

        return this.insertCode(code, record, forest);
      },
      {
        children: {},
      });
  }

  private insertCode(code: string, record: IPhoneCodeRecord, forest: IPhoneCodesForest, index = 0): IPhoneCodesForest {
    const digit = code[index];
    const result = {
      ...forest,
    };

    if (isNil(digit)) {
      return this.insertRecord(record, result);
    }

    if (!result.children[digit]) {
      result.children[digit] = {
        children: {},
      };
    }

    result.children[digit] = this.insertCode(code, record, result.children[digit], index + 1);

    return result;
  }

  private insertRecord(record: IPhoneCodeRecord, forest: IPhoneCodesForest): IPhoneCodesForest {
    if (!forest.record) {
      return {
        ...forest,
        record,
      };
    }

    if (forest.record.mask.length < record.mask.length) {
      return {
        ...forest,
        record,
      };
    }

    return forest;
  }

}
