import {isEqual} from '../util/utils';

export function Pure() {
  let cache: any;

  // tslint:disable-next-line:only-arrow-functions
  return function(target: any, propertyKey: string, descriptor: PropertyDescriptor): void {
    const original = descriptor.get;

    if (!original) {
      return;
    }

    // tslint:disable-next-line:only-arrow-functions
    descriptor.get = function() {
      // @ts-ignore
      const value = original.call(this);

      if (!isEqual(value, cache)) {
        cache = value;
      }

      return cache;
    };
  };
}
