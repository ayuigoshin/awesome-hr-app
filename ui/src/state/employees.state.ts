import {
  Add,
  CreateOrReplace,
  defaultEntityState,
  EntityState,
  EntityStateModel,
  IdStrategy, Payload,
  Remove, SetActive,
  SetLoading,
  Update,
} from '@ngxs-labs/entity-state';
import {Action, Selector, State, StateContext, Store} from '@ngxs/store';
import {IEmployee, IEmployeeCreate} from '../model/employee';
import {EmployeeService} from '../services/employee.service';
import {
  EmployeeAdd,
  EmployeeCreate,
  EmployeeDelete,
  EmployeeLoad,
  EmployeePatch,
  EmployeesLoad, EmployeesSearch, EmployeesSearchSuccess,
  EmployeeUpdate,
} from '../actions/employees.actions';
import {map, tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {Order, OrderBy} from '../model/order';
import {isNil, merge} from '../util/utils';
import {TagRemove} from '../actions/tags.actions';

export interface IEmployeesState extends EntityStateModel<IEmployee> {
  totalCount: number | null;
  loadingActive: boolean;
  query: {
    query?: string;
    offset?: number;
    limit?: number;
    tags?: string[];
    orderBy?: OrderBy<IEmployee>;
    order?: Order;
  };
  newEmployee: null | Partial<IEmployeeCreate>;
  creating: boolean;
}

@State<IEmployeesState>({
  name: 'employees',
  defaults: {
    ...defaultEntityState(),
    totalCount: null,
    loadingActive: false,
    query: {},
    newEmployee: null,
    creating: false,
  },
})
export class EmployeesState extends EntityState<IEmployee> {
  constructor(private readonly store: Store, private readonly employeeService: EmployeeService) {
    super(EmployeesState, 'id', IdStrategy.EntityIdGenerator);
  }

  @Selector()
  static totalCount(state: IEmployeesState) {
    return state.totalCount;
  }

  @Selector()
  static tags(state: IEmployeesState) {
    return state.query && state.query.tags ? state.query.tags : [];
  }

  @Selector()
  static active(state: IEmployeesState) {
    const {newEmployee, entities, active} = state;

    if (newEmployee) {
      return newEmployee;
    }

    return active === undefined ? null : entities[active];
  }

  @Selector()
  static creating(state: IEmployeesState) {
    return state.creating;
  }

  @Action(EmployeesLoad, {cancelUncompleted: true})
  loadEmployees(ctx: StateContext<IEmployeesState>, action: EmployeesLoad) {
    ctx.dispatch(new SetLoading(EmployeesState, true));
    ctx.patchState({
      query: merge(ctx.getState().query, action.payload),
    });

    const {query, tags, offset, limit, order, orderBy} = ctx.getState().query;

    return this.employeeService.getAll(query, offset, limit, tags, orderBy, order).pipe(
      tap(({totalCount, items}) => {
        ctx.patchState({
          entities: {},
          ids: [],
          error: undefined,
          totalCount,
        });
        ctx.dispatch(new SetLoading(EmployeesState, false));
        ctx.dispatch(new CreateOrReplace(EmployeesState, items));
      }),
    );
  }

  @Action(EmployeeLoad)
  loadEmployee(ctx: StateContext<IEmployeesState>, action: EmployeeLoad) {
    const {id} = action.payload;

    ctx.patchState({
      loadingActive: true,
    });

    return this.employeeService.get(id).pipe(
      tap(employee => {
        ctx.patchState({
          loadingActive: false,
        });
        ctx.dispatch(new CreateOrReplace(EmployeesState, [employee]));
      }),
    );
  }

  @Action(EmployeeDelete)
  deleteEmployee(ctx: StateContext<IEmployeesState>, action: EmployeeDelete) {
    const {id} = action.payload;

    ctx.patchState({
      loadingActive: true,
    });
    ctx.dispatch(new Remove(EmployeesState, employee => employee.id === id));

    return this.employeeService.delete(id).pipe(tap(() => {
      const totalCount = ctx.getState().totalCount;

      if (totalCount !== null) {
        ctx.patchState({
          totalCount: totalCount - 1,
        });
      }
    }));
  }

  @Action(EmployeeUpdate, {cancelUncompleted: true})
  updateEmployee(ctx: StateContext<IEmployeesState>, action: EmployeeUpdate) {
    const {id, employee} = action.payload;

    ctx.dispatch(new CreateOrReplace(EmployeesState, {...employee, id}));

    return this.employeeService.update(id, employee);
  }

  @Action(EmployeePatch, {cancelUncompleted: true})
  patchEmployee(ctx: StateContext<IEmployeesState>, action: EmployeePatch) {
    const {id, employee} = action.payload;

    ctx.dispatch(new Update(EmployeesState, employee => employee.id === id, employee));

    return this.employeeService.patch(id, employee);
  }

  @Action(EmployeeCreate, {cancelUncompleted: true})
  createEmployee(ctx: StateContext<IEmployeesState>, action: EmployeeCreate) {
    const {employee} = action.payload;

    ctx.patchState({creating: true});

    return this.employeeService.create(employee).pipe(tap(employee => {
      ctx.dispatch(new Add(EmployeesState, employee));
      ctx.patchState({creating: false, newEmployee: null});
      ctx.dispatch(new SetActive(EmployeesState, String(employee.id)));
    }));
  }

  @Action(EmployeeAdd)
  addEmployee(ctx: StateContext<IEmployeesState>, action: EmployeeAdd) {

    ctx.patchState({newEmployee: {}});
  }

  @Action(TagRemove)
  removeTag(ctx: StateContext<IEmployeesState>, action: TagRemove) {
    const employees = Object.values(ctx.getState().entities);

    employees.forEach(employee => {
      if (employee.tags) {
        ctx.dispatch(new Update(EmployeesState, entity => entity.id === employee.id, {
          ...employee,
          tags: employee.tags.filter(tag => tag !== action.payload.tag),
        }));
      }
    });
  }

  @Action(EmployeesSearch, {cancelUncompleted: true})
  searchTags(ctx: StateContext<IEmployeesState>, action: EmployeesSearch) {
    const {query, tags, offset, limit, order, orderBy} = action.payload;

    return this.employeeService.getAll(query, offset, limit, tags, orderBy, order).pipe(
      tap(({totalCount, items}) => {
        ctx.dispatch(new EmployeesSearchSuccess({
          searchKey: action.payload.searchKey,
          employees: items,
          totalCount,
        }));
      }),
    );
  }

  // override
  clearActive(ctx: StateContext<IEmployeesState>): void {
    super.clearActive(ctx);
    ctx.patchState({creating: false, newEmployee: null});
  }

  // override
  setActive(ctx: StateContext<IEmployeesState>, action: Payload<string>): void {
    super.setActive(ctx, action);
    ctx.patchState({creating: false, newEmployee: null});
  }
}
