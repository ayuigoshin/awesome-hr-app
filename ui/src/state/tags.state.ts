import {Action, Selector, State, StateContext, Store} from '@ngxs/store';
import {tap} from 'rxjs/operators';
import {TagRemove, TagsAdd, TagsLoad} from '../actions/tags.actions';
import {TagService} from '../services/tag.service';
import {EmployeeAdd, EmployeeCreate, EmployeePatch, EmployeeUpdate} from '../actions/employees.actions';
import {IEmployee} from '../model/employee';

export interface ITagsState {
  tags: string[];
  totalCount: number | null;
  loading: boolean;
}

@State<ITagsState>({
  name: 'tags',
  defaults: {
    totalCount: null,
    tags: [],
    loading: false,
  },
})
export class TagsState {
  constructor(private readonly store: Store, private readonly tagsService: TagService) {

  }

  @Selector()
  static totalCount(state: ITagsState) {
    return state.totalCount;
  }

  @Selector()
  static tags(state: ITagsState) {
    return state.tags;
  }

  @Action(TagsLoad)
  loadTags(ctx: StateContext<ITagsState>, action: TagsLoad) {
    ctx.patchState({
      loading: true,
      tags: [],
      totalCount: null,
    });

    const {query, offset, limit} = action.payload;

    return this.tagsService.getAll(query, offset, limit).pipe(
      tap(({totalCount, items}) => {
        ctx.patchState({
          totalCount,
          tags: items,
          loading: false,
        });
      }),
    );
  }

  @Action(TagRemove)
  removeTag(ctx: StateContext<ITagsState>, action: TagRemove) {
    const {tag} = action.payload;
    const tags = ctx.getState().tags.filter(candidate => candidate !== tag);


    ctx.patchState({
      tags,
    });

    return this.tagsService.remove(tag).pipe(tap(() => {
      const totalCount = ctx.getState().totalCount;

      if (totalCount !== null) {
        ctx.patchState({
          totalCount: totalCount - 1,
        });
      }
    }));
  }

  @Action(TagsAdd)
  addTags(ctx: StateContext<ITagsState>, action: TagsAdd) {
    const {tags} = action.payload;
    const oldTags = ctx.getState().tags;
    const newTags = tags.filter(candidate => !oldTags.includes(candidate));
    const allTags = oldTags.concat(newTags);


    ctx.patchState({
      tags: allTags,
    });

    return this.tagsService.add(newTags).pipe(tap(() => {
      const totalCount = ctx.getState().totalCount;

      if (totalCount !== null) {
        ctx.patchState({
          totalCount: totalCount + newTags.length,
        });
      }
    }));
  }


  @Action(EmployeeCreate)
  onAddEmployee(ctx: StateContext<ITagsState>, action: EmployeeCreate) {
    this.addTagFromEmployee(ctx, action.payload.employee);
  }

  @Action(EmployeeUpdate)
  onUpdateEmployee(ctx: StateContext<ITagsState>, action: EmployeeUpdate) {
    this.addTagFromEmployee(ctx, action.payload.employee);
  }

  @Action(EmployeePatch)
  onPatchEmployee(ctx: StateContext<ITagsState>, action: EmployeePatch) {
    this.addTagFromEmployee(ctx, action.payload.employee);
  }

  private addTagFromEmployee(ctx: StateContext<ITagsState>, employee: Partial<IEmployee>) {
    if (!employee.tags) {
      return;
    }

    const oldTags = ctx.getState().tags;
    const newTags = employee.tags.filter(candidate => !oldTags.includes(candidate));

    if (!newTags.length) {
      return;
    }

    ctx.patchState({
      tags: [...oldTags, ...newTags],
    });
  }

}
