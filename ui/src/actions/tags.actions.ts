export class TagsLoad {
  static readonly type = '[Tags] Load';

  constructor(readonly payload: {
    query?: string,
    offset?: number,
    limit?: number,
  } = {}) {}
}

export class TagsAdd {
  static readonly type = '[Tag] Add';

  constructor(readonly payload: {tags: string[]}) {}
}

export class TagRemove {
  static readonly type = '[Tag] Remove';

  constructor(readonly payload: {tag: string}) {}
}


