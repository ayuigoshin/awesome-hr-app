import {Order, OrderBy} from '../model/order';
import {IEmployee, IEmployeeCreate} from '../model/employee';
import {SetActive} from '@ngxs-labs/entity-state';

export class EmployeesLoad {
  static readonly type = '[Employees] Load';

  constructor(readonly payload: {
    query?: string | null,
    offset?: number | null,
    limit?: number | null,
    tags?: string[] | null,
    orderBy?: OrderBy<IEmployee> | null,
    order?: Order | null
  }) {
  }
}

export class EmployeesSearch {
  static readonly type = '[Employees] Search';

  constructor(readonly payload: {
    searchKey: string,
    query?: string,
    offset?: number,
    limit?: number,
    tags?: string[],
    orderBy?: OrderBy<IEmployee>,
    order?: Order
  }) {
  }
}

export class EmployeesSearchSuccess {
  static readonly type = '[Employees] Search success';

  constructor(readonly payload: {
    searchKey: string,
    employees: IEmployee[],
    totalCount: number,
  }) {
  }
}

export class EmployeeLoad {
  static readonly type = '[Employee] Load';

  constructor(readonly payload: { id: number }) {
  }
}


export class EmployeeDelete {
  static readonly type = '[Employee] Delete';

  constructor(readonly payload: { id: number }) {
  }
}

export class EmployeeCreate {
  static readonly type = '[Employee] Create';

  constructor(readonly payload: { employee: IEmployeeCreate }) {
  }
}

export class EmployeeUpdate {
  static readonly type = '[Employee] Update';

  constructor(readonly payload: { id: number, employee: IEmployee }) {
  }
}

export class EmployeePatch {
  static readonly type = '[Employee] Patch';

  constructor(readonly payload: { id: number, employee: Partial<IEmployee> }) {
  }
}

// open create form
export class EmployeeAdd {
  static readonly type = '[Employee] Add';
}

