import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {EmployeesPageModule} from '../employeesPage/employeesPage.module';
import {AppBarModule} from '../../components/app-bar/appBar.module';
import {TagService} from '../../services/tag.service';
import {NgxsModule} from '@ngxs/store';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    EmployeesPageModule,
    AppBarModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgxsModule.forRoot([]),
  ],
  providers: [TagService],
  bootstrap: [AppComponent]
})
export class AppModule { }
