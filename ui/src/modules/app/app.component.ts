import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'app',
  templateUrl: './app.template.html',
  styleUrls: ['./app.style.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
}
