import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {EmployeesPageComponent} from '../employeesPage/employeesPage.component';


const routes: Routes = [
  {
    path: '',
    component: EmployeesPageComponent,
  },
  {
    path: 'employee/:id',
    component: EmployeesPageComponent,
  },
  {
    path: '*',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
