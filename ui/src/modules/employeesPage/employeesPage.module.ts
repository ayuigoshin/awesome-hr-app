import { NgModule } from '@angular/core';
import { EmployeesPageComponent } from './employeesPage.component';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {NgxsModule} from '@ngxs/store';
import {EmployeesSmartModule} from '../../smart-components/employees/employees.smart.module';
import {EmployeeService} from '../../services/employee.service';
import {EmployeeSearchSmartModule} from '../../smart-components/employeeSearch/employeeSearch.smart.module';
import {EmployeeDetailsSmartModule} from '../../smart-components/employeeDetails/employeeDetails.smart.module';
import {CommonModule} from '@angular/common';
import {AdaptiveService} from '../../services/adaptive.service';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MapperPipeModule} from '../../pipes/mapper/mapper.pipe.module';
import {EmployeesState} from '../../state/employees.state';
import {TagsState} from '../../state/tags.state';
import {MatTooltipModule} from '@angular/material/tooltip';
import {TagsManagementSmartModule} from '../../smart-components/tagsManagement/tagsManagement.smart.module';

@NgModule({
  imports: [
    CommonModule,
    MapperPipeModule,
    NgxsModule.forFeature([EmployeesState, TagsState]),
    MatCardModule,
    EmployeesSmartModule,
    EmployeeDetailsSmartModule,
    EmployeeSearchSmartModule,
    MatButtonModule,
    MatIconModule,
    MatSidenavModule,
    MatTooltipModule,
    TagsManagementSmartModule,
  ],
  exports: [EmployeesPageComponent],
  declarations: [
    EmployeesPageComponent
  ],
  providers: [EmployeeService, AdaptiveService],
})
export class EmployeesPageModule { }
