import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Select, Store} from '@ngxs/store';
import {EmployeeAdd} from '../../actions/employees.actions';
import {AdaptiveService} from '../../services/adaptive.service';
import {Pure} from '../../decorators/pure.decorator';
import {EmployeesState} from '../../state/employees.state';
import {Observable} from 'rxjs';
import {ClearActive} from '@ngxs-labs/entity-state';
import {IEmployee} from '../../model/employee';
import {isNil, not} from '../../util/utils';
import {map} from 'rxjs/operators';
import {TagsManagementService} from '../../services/tagsManagement.service';

@Component({
  selector: 'employees-page',
  templateUrl: './employeesPage.template.html',
  styleUrls: ['./employeesPage.style.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [TagsManagementService],
})
export class EmployeesPageComponent {
  @Select(EmployeesState.active) active$: Observable<IEmployee | Partial<IEmployee> | null>;


  constructor(private readonly store: Store,
              private readonly adaptiveService: AdaptiveService,
              private readonly tagsManagementService: TagsManagementService) {

  }

  get tagsManagementOpened$(): Observable<boolean> {
    return this.tagsManagementService.open$;
  }

  get large$(): Observable<boolean> {
    return this.adaptiveService.l$;
  }

  @Pure()
  get small$(): Observable<boolean> {
    return this.adaptiveService.m$.pipe(map(not));
  }

  not(value: any): boolean {
    return not(value);
  }

  hasActive(active: IEmployee | Partial<IEmployee> | null): boolean {
    return !isNil(active);
  }

  onAddClick() {
    this.store.dispatch(new EmployeeAdd());
  }

  openChanged(opened: boolean) {
    if (!opened) {
      this.clearEmployee();
    }
  }

  closeTagsManagement() {
    this.tagsManagementService.close();
  }

  private clearEmployee() {
    this.store.dispatch(new ClearActive(EmployeesState));
  }
}
