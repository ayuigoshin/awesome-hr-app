import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Select, Store} from '@ngxs/store';
import {Observable} from 'rxjs';
import {TagsState} from '../../state/tags.state';
import {EmployeeCreate, EmployeesLoad, EmployeeUpdate} from '../../actions/employees.actions';
import {EmployeesState} from '../../state/employees.state';
import {IEmployee, IEmployeeCreate} from '../../model/employee';
import {isNil} from '../../util/utils';
import {TagRemove, TagsLoad} from '../../actions/tags.actions';

@Component({
  selector: 'employee-details-smart',
  styleUrls: ['employeeDetails.smart.style.less'],
  templateUrl: 'employeeDetails.smart.template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmployeeDetailsSmartComponent {
  @Select(TagsState.tags) allTags$: Observable<string[]>;
  @Select(EmployeesState.active) active$: Observable<IEmployee | IEmployeeCreate>;

  constructor(private readonly store: Store) {
  }

  onChange(employee: IEmployee | IEmployeeCreate) {
    if (this.isEmployee(employee) && !isNil(employee.id)) {
      this.store.dispatch(new EmployeeUpdate({id: employee.id, employee}));

      return;
    }

    return this.store.dispatch(new EmployeeCreate({employee}));
  }

  onSearchTags(query: string) {
    this.store.dispatch(new TagsLoad({query}));
  }

  onTagRemove(tag: string) {
    this.store.dispatch(new TagRemove({tag}));
  }

  private isEmployee(employee: IEmployee | IEmployeeCreate): employee is IEmployee {
    return 'id' in employee;
  }
}
