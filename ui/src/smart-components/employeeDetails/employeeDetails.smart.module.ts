import {NgModule} from '@angular/core';

import {EmployeeDetailsSmartComponent} from './employeeDetails.smart.component';
import {EmployeeSearchModule} from '../../components/employeeSearch/employeeSearch.module';
import {CommonModule} from '@angular/common';
import {EmployeeDetailsModule} from '../../components/employeeDetails/employeeDetails.module';

@NgModule({
  imports: [CommonModule, EmployeeDetailsModule],
  exports: [EmployeeDetailsSmartComponent],
  declarations: [EmployeeDetailsSmartComponent],
  providers: [],
})
export class EmployeeDetailsSmartModule {
}
