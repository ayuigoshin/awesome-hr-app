import {ChangeDetectionStrategy, Component, OnInit, Optional} from '@angular/core';
import {Select, Store} from '@ngxs/store';
import {Observable} from 'rxjs';
import {TagsState} from '../../state/tags.state';
import {TagRemove, TagsLoad} from '../../actions/tags.actions';
import {EmployeesLoad} from '../../actions/employees.actions';
import {EmployeesState} from '../../state/employees.state';
import {TagsManagementService} from '../../services/tagsManagement.service';

@Component({
  selector: 'employee-search-smart',
  styleUrls: ['employeeSearch.smart.style.less'],
  templateUrl: 'employeeSearch.smart.template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmployeeSearchSmartComponent {
  @Select(TagsState.tags) allTags$: Observable<string[]>;
  @Select(EmployeesState.tags) tags$: Observable<string[]>;

  constructor(private readonly store: Store,
              @Optional() private readonly tagsManagementService: TagsManagementService) {
  }


  onSearch(query: string) {
    this.store.dispatch(new EmployeesLoad({query, limit: 20}));
  }

  onTagsChanged(tags: string[]) {
    this.store.dispatch(new EmployeesLoad({tags}));
  }

  searchTags(query: string) {
    this.store.dispatch(new TagsLoad({query}));
  }

  onTagRemove(tag: string) {
    this.store.dispatch(new TagRemove({tag}));
  }

  manageTags() {
    if (this.tagsManagementService) {
      this.tagsManagementService.open();
    }
  }
}
