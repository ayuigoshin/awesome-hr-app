import {NgModule} from '@angular/core';

import {EmployeeSearchSmartComponent} from './employeeSearch.smart.component';
import {EmployeeSearchModule} from '../../components/employeeSearch/employeeSearch.module';
import {CommonModule} from '@angular/common';

@NgModule({
  imports: [CommonModule, EmployeeSearchModule],
  exports: [EmployeeSearchSmartComponent],
  declarations: [EmployeeSearchSmartComponent],
  providers: [],
})
export class EmployeeSearchSmartModule {
}
