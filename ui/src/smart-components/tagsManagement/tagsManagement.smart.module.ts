import {NgModule} from '@angular/core';

import {TagsManagementSmartComponent} from './tagsManagement.smart.component';
import {EmployeeSearchModule} from '../../components/employeeSearch/employeeSearch.module';
import {CommonModule} from '@angular/common';
import {TagsManagementModule} from '../../components/tagsManagement/tagsManagement.module';
import {MatDialogModule} from '@angular/material/dialog';
import {EmployeesSelectDialogSmartModule} from '../employeesSelectDialog/employeesSelectDialog.smart.module';

@NgModule({
  imports: [CommonModule, TagsManagementModule, MatDialogModule, EmployeesSelectDialogSmartModule],
  exports: [TagsManagementSmartComponent],
  declarations: [TagsManagementSmartComponent],
  providers: [],
})
export class TagsManagementSmartModule {
}
