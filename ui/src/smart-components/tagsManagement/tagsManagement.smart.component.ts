import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Select, Store} from '@ngxs/store';
import {Observable} from 'rxjs';
import {TagsState} from '../../state/tags.state';
import {TagRemove, TagsAdd, TagsLoad} from '../../actions/tags.actions';
import {MatDialog} from '@angular/material/dialog';
import {EmployeesSelectDialogSmartComponent} from '../employeesSelectDialog/employeesSelectDialog.smart.component';
import {IEmployee} from '../../model/employee';
import {Update} from '@ngxs-labs/entity-state';
import {EmployeesState} from '../../state/employees.state';
import {EmployeeUpdate} from '../../actions/employees.actions';

@Component({
  selector: 'tags-management-smart',
  styleUrls: ['tagsManagement.smart.style.less'],
  templateUrl: 'tagsManagement.smart.template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TagsManagementSmartComponent implements OnInit {
  @Select(TagsState.tags) tags$: Observable<string[]>;

  constructor(private readonly store: Store,
              private readonly dialog: MatDialog) {
  }

  ngOnInit() {
    this.store.dispatch(new TagsLoad({}));
  }

  onAssign(tags: string[]) {
    this.dialog.open(EmployeesSelectDialogSmartComponent, {
      width: '500px',
      data: {title: 'Select employees to assign tags'},
    }).afterClosed()
      .subscribe((employees: IEmployee[] | null) => {
        if (employees) {
          employees.forEach(employee => {
            const updatedEmployee = this.addEmployeeTags(tags, employee);

            this.store.dispatch(new EmployeeUpdate({id: employee.id, employee: updatedEmployee}));
          });
        }
      });
  }

  onTagRemove(tag: string) {
    this.store.dispatch(new TagRemove({tag}));
  }

  addTag(tag: string) {
    this.store.dispatch(new TagsAdd({tags: [tag]}));
  }

  private addEmployeeTags(tags: string[], employee: IEmployee): IEmployee {
    const updated = {
      ...employee,
    };

    tags.forEach(tag => {
      if (!updated.tags) {
        updated.tags = [tag];

        return;
      }

      if (!updated.tags.includes(tag)) {
        updated.tags.push(tag);
      }
    });

    return updated;
  }
}
