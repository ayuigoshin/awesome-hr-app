import {NgModule} from '@angular/core';
import {EmployeesSelectDialogSmartComponent} from './employeesSelectDialog.smart.component';
import {EmployeeService} from '../../services/employee.service';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {FilterPipeModule} from '../../pipes/filter/filter.pipe.module';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FilterPipeModule,
    MatDialogModule,
    MatButtonModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatIconModule,
    MatInputModule,
  ],
  exports: [EmployeesSelectDialogSmartComponent],
  declarations: [
    EmployeesSelectDialogSmartComponent,
  ],
  entryComponents: [EmployeesSelectDialogSmartComponent],
  providers: [],
})
export class EmployeesSelectDialogSmartModule {
}
