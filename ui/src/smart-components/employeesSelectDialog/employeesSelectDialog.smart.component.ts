import {ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Actions, ofActionDispatched, Store} from '@ngxs/store';
import {FormControl} from '@angular/forms';
import {Observable, Subject} from 'rxjs';
import {debounceTime, filter, map, startWith, takeUntil} from 'rxjs/operators';
import {EmployeesSearch, EmployeesSearchSuccess} from '../../actions/employees.actions';
import {Pure} from '../../decorators/pure.decorator';
import {IEmployee} from '../../model/employee';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {IConfirmation} from '../../components/confirmation/confirmation.interface';

@Component({
  selector: 'employees-select-dialog-smart',
  templateUrl: './employeesSelectDialog.smart.template.html',
  styleUrls: ['./employeesSelectDialog.smart.style.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmployeesSelectDialogSmartComponent implements OnInit, OnDestroy {
  readonly searchKey = Math.random().toString(36).substring(7);
  readonly searchControl = new FormControl('');
  readonly selected: IEmployee[] = [];

  private readonly destroy$ = new Subject();

  constructor(private readonly store: Store,
              private readonly actions$: Actions,
              @Inject(MAT_DIALOG_DATA) public data: {title: string}) {
  }

  @Pure()
  get employees$(): Observable<IEmployee[]> {
    return this.actions$.pipe(
      ofActionDispatched(EmployeesSearchSuccess),
      filter(({payload: {searchKey}}) => searchKey === this.searchKey),
      map(({payload: {employees}}) => employees),
      startWith([]),
    );
  }

  ngOnInit() {
    this.listenSearch();
  }

  isAvailable(employee: IEmployee, selected: IEmployee[]): boolean {
    return !selected.find(candidate => candidate.id === employee.id);
  }

  remove(employee: IEmployee) {
    const index = this.selected.findIndex(item => item.id === employee.id);

    this.selected.splice(index, 1);
  }

  onOptionSelected({option: {value}}: MatAutocompleteSelectedEvent) {
    this.addEmployee(value);
  }


  ngOnDestroy() {
    this.destroy$.next();
  }

  private addEmployee(employee: IEmployee) {
    const found = this.selected.find(candidate => candidate.id === employee.id);

    if (found) {
      return;
    }

    this.selected.push(employee);
  }

  private listenSearch() {
    this.searchControl.valueChanges.pipe(
      startWith(''),
      debounceTime(300),
      takeUntil(this.destroy$),
    ).subscribe(value => {
      this.store.dispatch(new EmployeesSearch({query: value, searchKey: this.searchKey}));
    });
  }

}
