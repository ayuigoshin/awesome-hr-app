import {NgModule} from '@angular/core';
import {EmployeesSmartComponent} from './employees.smart.component';
import {EmployeesModule} from '../../components/employees/employees.module';
import {CommonModule} from '@angular/common';
import {LoaderModule} from '../../components/loader/loader.module';
import {AdaptiveService} from '../../services/adaptive.service';

@NgModule({
  imports: [CommonModule, EmployeesModule, LoaderModule],
  exports: [EmployeesSmartComponent],
  declarations: [EmployeesSmartComponent],
  providers: [AdaptiveService],
})
export class EmployeesSmartModule {
}
