import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Select, Store} from '@ngxs/store';
import {EmployeesState} from '../../state/employees.state';
import {Observable} from 'rxjs';
import {IEmployee} from '../../model/employee';
import {EmployeeDelete, EmployeesLoad} from '../../actions/employees.actions';
import {distinctUntilChanged, map, take} from 'rxjs/operators';
import {Sort} from '@angular/material/sort';
import {Order, OrderBy} from '../../model/order';
import {SetActive} from '@ngxs-labs/entity-state';


@Component({
  selector: 'employees-smart',
  styleUrls: ['employees.smart.style.less'],
  templateUrl: 'employees.smart.template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmployeesSmartComponent implements OnInit {
  @Select(EmployeesState.loading) loading$: Observable<boolean>;
  @Select(EmployeesState.entities) employees$: Observable<IEmployee[]>;
  @Select(EmployeesState.totalCount) totalCount$: Observable<number>;
  @Select(EmployeesState.tags) tags$: Observable<string[]>;

  constructor(private readonly store: Store) {

  }

  ngOnInit() {
    this.store.dispatch(new EmployeesLoad({}));
  }

  onLimitChanges(limit: number) {
    this.store.dispatch(new EmployeesLoad({limit}));
  }

  onOffsetChanged(offset: number) {
    this.store.dispatch(new EmployeesLoad({offset}));
  }

  onTagClicked(tag: string) {
    this.tags$.pipe(take(1))
      .subscribe(tags => {
        if (!tags.includes(tag)) {
          this.store.dispatch(new EmployeesLoad({tags: [...tags, tag]}));
        }
      });
  }

  onSort({direction, active}: Sort) {
    if (active && direction) {
      this.store.dispatch(new EmployeesLoad({
        orderBy: active as OrderBy<IEmployee>,
        order: direction.toUpperCase() as Order,
      }));

      return;
    }

    this.store.dispatch(new EmployeesLoad({
      orderBy: null,
      order: null,
    }));
  }

  onSelected(employee: IEmployee) {
    this.store.dispatch(new SetActive(EmployeesState, String(employee.id)));
  }

  onRemove(employee: IEmployee) {
    this.store.dispatch(new EmployeeDelete({id: employee.id}));
  }
}
