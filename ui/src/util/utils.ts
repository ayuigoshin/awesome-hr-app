import {HttpParams} from '@angular/common/http';

/**
 * Checks if `value` is `null` or `undefined`.
 *
 * @returns boolean Returns `true` if `value` is nullish, else `false`.
 * @example
 *
 * _.isNil(null);
 * // => true
 *
 * _.isNil(void 0);
 * // => true
 *
 * _.isNil(NaN);
 * // => false
 */
export function isNil(value: any): value is null | undefined {
  return value === undefined || value === null;
}

export function not(value: any): boolean {
  return !value;
}

export function valueOrDefault<T, R>(value: T, defaultValue: R): T | R {
  return isNil(value) ? defaultValue : value;
}

export function isEqual<T>(value: T, anotherValue: T): boolean {
  return JSON.stringify(value) === JSON.stringify(anotherValue);
}

export function toHttpParams(obj: any): HttpParams {
  const params = Object.keys(obj).reduce((acc, key) => {
    if (obj.hasOwnProperty(key) && !isNil(obj[key]) && obj[key] !== '') {
      return {
        ...acc,
        [key]: String(obj[key]),
      };
    }

    return acc;
  }, {});

  return new HttpParams({
    fromObject: params,
  });
}

export function merge<T extends object, R extends object>(src1: T, src2: R): T & R {
  return Object.assign({}, src1, src2);
}
