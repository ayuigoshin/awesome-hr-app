import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatSelectionListChange} from '@angular/material/list';
import {ConfirmationComponent} from '../confirmation/confirmation.component';
import {MatDialog} from '@angular/material/dialog';
import {ISelection} from '../../model/selection';
import {ENTER, SPACE} from '@angular/cdk/keycodes';

@Component({
  selector: 'tags-management',
  styleUrls: ['tagsManagement.style.less'],
  templateUrl: 'tagsManagement.template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TagsManagementComponent {
  @Input() tags: string[] = [];

  @Output() assign = new EventEmitter<string[]>();
  @Output() tagRemove = new EventEmitter<string>();
  @Output() tagAdd = new EventEmitter<string>();

  selected: ISelection = {};

  readonly searchControl = new FormControl('');
  readonly newTagControl = new FormControl('');

  constructor(private readonly dialog: MatDialog) {
  }

  getSelectedTags(selected: ISelection) {
    return Object.keys(selected).filter(tag => selected[tag] === true);
  }

  filterByQuery(tag: string, query: string): boolean {
    return tag.includes(query);
  }

  clearSearch() {
    this.searchControl.reset('');
  }

  assignTags() {
    const tags = this.getSelectedTags(this.selected);

    this.assign.emit(tags);
  }

  onRemoveTag(event: MouseEvent, tag: string) {
    event.stopPropagation();

    this.dialog.open(ConfirmationComponent, {
      width: '500px',
      data: {text: `Are you sure, you want to remove tag "${tag}"?`},
    }).afterClosed()
      .subscribe(remove => {
        if (remove) {
          this.tagRemove.emit(tag);
        }
      });
  }

  onSelectionChange({option: {selected, value}}: MatSelectionListChange) {
    this.selected = {
      ...this.selected,
      [value]: selected,
    };
  }

  onRemoveSelected() {
    this.dialog.open(ConfirmationComponent, {
      width: '500px',
      data: {text: `Are you sure, you want to remove selected tags?`},
    }).afterClosed()
      .subscribe(remove => {
        if (remove) {
          this.getSelectedTags(this.selected).forEach(tag => {
            this.tagRemove.emit(tag);
          });
        }
      });

  }

  addNewTag() {
    const tag = this.newTagControl.value.trim();

    if (tag) {
      this.tagAdd.emit(tag);
      this.newTagControl.reset('', {emitEvent: false});
    }
  }
}
