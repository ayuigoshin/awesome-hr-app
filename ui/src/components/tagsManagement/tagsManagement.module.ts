import {NgModule} from '@angular/core';

import {TagsManagementComponent} from './tagsManagement.component';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatButtonModule} from '@angular/material/button';
import {MapperPipeModule} from '../../pipes/mapper/mapper.pipe.module';
import {PhoneMaskService} from '../../services/phoneMask.service';
import {MatListModule} from '@angular/material/list';
import {FilterPipeModule} from '../../pipes/filter/filter.pipe.module';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatCheckboxModule} from '@angular/material/checkbox';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatListModule,
    MatInputModule,
    FilterPipeModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    MapperPipeModule,
    MatCheckboxModule,
  ],
  exports: [TagsManagementComponent],
  declarations: [TagsManagementComponent],
  providers: [MatDatepickerModule, PhoneMaskService],
})
export class TagsManagementModule {
}
