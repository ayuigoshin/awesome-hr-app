import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {Store} from '@ngxs/store';
import {EmployeeAdd} from '../../actions/employees.actions';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {IConfirmation} from './confirmation.interface';

const defaultData = {
  acceptText: 'Yes',
  cancelText: 'No',
};

@Component({
  selector: 'employees-page',
  templateUrl: './confirmation.template.html',
  styleUrls: ['./confirmation.style.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfirmationComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: IConfirmation) {
    this.data = Object.assign({}, defaultData, data);
  }


}
