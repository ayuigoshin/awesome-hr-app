export interface IConfirmation {
  text: string;
  acceptText?: string;
  cancelText?: string;
}
