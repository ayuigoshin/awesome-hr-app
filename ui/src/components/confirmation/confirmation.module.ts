import {NgModule} from '@angular/core';
import {ConfirmationComponent} from './confirmation.component';
import {EmployeeService} from '../../services/employee.service';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  imports: [
    MatDialogModule,
    MatButtonModule,
  ],
  exports: [ConfirmationComponent],
  declarations: [
    ConfirmationComponent,
  ],
  entryComponents: [ConfirmationComponent],
  providers: [EmployeeService],
})
export class ConfirmationModule {
}
