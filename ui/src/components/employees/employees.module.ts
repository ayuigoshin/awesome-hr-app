import {NgModule} from '@angular/core';
import {MatTableModule} from '@angular/material/table';
import {EmployeesComponent} from './employees.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatChipsModule} from '@angular/material/chips';
import {CommonModule} from '@angular/common';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatRippleModule} from '@angular/material/core';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {ConfirmationModule} from '../confirmation/confirmation.module';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTooltipModule} from '@angular/material/tooltip';
import {AdaptiveService} from '../../services/adaptive.service';
import {MapperPipeModule} from '../../pipes/mapper/mapper.pipe.module';

@NgModule({
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatChipsModule,
    MatExpansionModule,
    MatRippleModule,
    MatIconModule,
    MatButtonModule,
    ConfirmationModule,
    MatDialogModule,
    MatTooltipModule,
    MapperPipeModule,
  ],
  exports: [EmployeesComponent],
  declarations: [EmployeesComponent],
  providers: [AdaptiveService],
})
export class EmployeesModule {
}
