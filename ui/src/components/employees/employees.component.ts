import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Sort} from '@angular/material/sort';
import {PageEvent} from '@angular/material/paginator';
import {IEmployee} from '../../model/employee';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmationComponent} from '../confirmation/confirmation.component';
import {AdaptiveService} from '../../services/adaptive.service';
import {Observable} from 'rxjs';
import {Pure} from '../../decorators/pure.decorator';

@Component({
  selector: 'employees',
  styleUrls: ['employees.style.less'],
  templateUrl: 'employees.template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmployeesComponent {
  @Input() employees = [];
  @Input() displayedColumns = ['name', 'birthDate', 'phone', 'location', 'tags', 'remove'];
  @Input() totalCount = this.employees.length;

  @Output() limitChanged = new EventEmitter<number>();
  @Output() offsetChanged = new EventEmitter<number>();
  @Output() tagClicked = new EventEmitter<string>();
  @Output() sort = new EventEmitter<Sort>();
  @Output() employeeClicked = new EventEmitter<IEmployee>();
  @Output() remove = new EventEmitter<IEmployee>();

  constructor(private readonly dialog: MatDialog,
              private readonly adaptiveService: AdaptiveService) {
  }

  @Pure()
  get large(): Observable<boolean> {
    return this.adaptiveService.l$;
  }

  formatDateString(dateString: string): string {
    return dateString ? new Date(dateString).toLocaleDateString() : '';
  }

  changeLimit(limit: number) {
    this.limitChanged.emit(limit);
  }

  changeOffset(offset: number) {
    this.offsetChanged.emit(offset);
  }

  onTagClicked(event: Event, tag: string) {
    event.stopPropagation();
    this.tagClicked.emit(tag);
  }

  sortData(sort: Sort) {
    this.sort.emit(sort);
  }

  onPageEvent({pageSize, pageIndex}: PageEvent) {
    this.changeOffset(pageSize * pageIndex);
    this.changeLimit(pageSize);
  }

  onEmployeeClicked(employee: IEmployee) {
    this.employeeClicked.emit(employee);
  }

  onRemove(event: Event, employee: IEmployee) {
    event.stopPropagation();

    this.dialog.open(ConfirmationComponent, {
      width: '500px',
      data: {text: `Are you sure, you want to remove "${employee.name}" from the list?`},
    }).afterClosed()
      .subscribe(remove => {
        if (remove) {
          this.remove.emit(employee);
        }
      });
  }
}
