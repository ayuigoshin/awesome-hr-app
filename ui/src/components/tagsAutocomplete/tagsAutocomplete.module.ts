import {NgModule} from '@angular/core';

import {TagsAutocompleteComponent} from './tagsAutocomplete.component';
import {MatInputModule} from '@angular/material/input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {MatChipsModule} from '@angular/material/chips';
import {CommonModule} from '@angular/common';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {FilterPipeModule} from '../../pipes/filter/filter.pipe.module';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {ConfirmationModule} from '../confirmation/confirmation.module';
import {MatDialogModule} from '@angular/material/dialog';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatInputModule,
    MatIconModule,
    MatChipsModule,
    MatAutocompleteModule,
    FilterPipeModule,
    MatButtonModule,
    MatDialogModule,
    ConfirmationModule,
  ],
  exports: [TagsAutocompleteComponent],
  declarations: [TagsAutocompleteComponent],
  providers: [],
})
export class TagsAutocompleteModule {
}
