import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input, OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatChipInputEvent} from '@angular/material/chips';
import {ENTER, SPACE} from '@angular/cdk/keycodes';
import {MatAutocompleteSelectedEvent, MatAutocompleteTrigger} from '@angular/material/autocomplete';
import {Subject} from 'rxjs';
import {debounceTime, startWith, takeUntil} from 'rxjs/operators';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmationComponent} from '../confirmation/confirmation.component';

@Component({
  selector: 'tags-autocomplete',
  styleUrls: ['tagsAutocomplete.style.less'],
  templateUrl: 'tagsAutocomplete.template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TagsAutocompleteComponent implements OnInit, OnDestroy {
  @Input()
  tags: string[] = [];
  @Input()
  selected: string[] = [];
  @Input()
  placeholder = 'Tags';

  @ViewChild('tagsInput') tagsInput: ElementRef<HTMLInputElement>;
  @ViewChild('tagsInput', {read: MatAutocompleteTrigger}) inputAutoComplete: MatAutocompleteTrigger;


  readonly control = new FormControl('');

  readonly separatorKeysCodes: number[] = [ENTER, SPACE];
  readonly destroy$ = new Subject();

  @Output() tagRemove = new EventEmitter<string>();
  @Output() tagsChanged = new EventEmitter<string[]>();
  @Output() searchTags = new EventEmitter<string>();

  constructor(private readonly dialog: MatDialog) {
  }

  ngOnInit() {
    this.listenTagsSearch();
  }

  isAvailable(tag: string, query: string, usedTags: string): boolean {
    return !usedTags.includes(tag) && tag.includes(query);
  }

  removeTag(tag: string) {
    this.selected = this.selected.filter(item => item !== tag);
    this.tagsChanged.emit(this.selected);
  }

  onTokenEnd({value}: MatChipInputEvent) {
    this.addTag(value);
  }

  onOptionSelected({option: {value}}: MatAutocompleteSelectedEvent) {
    this.addTag(value);
  }

  onRemove(event: MouseEvent, tag: string) {
    event.stopPropagation();

    this.dialog.open(ConfirmationComponent, {
      width: '500px',
      data: {text: `Are you sure, you want to remove tag "${tag}"?`},
    }).afterClosed()
      .subscribe(remove => {
        if (remove) {
          this.tagRemove.emit(tag);
        }
      });
  }

  onInputClick() {
    if (!this.tags.length) {
      this.searchTags.emit(this.control.value);
    }
    this.openAutocomplete();
  }


  ngOnDestroy() {
    this.destroy$.next();
  }

  private resetInputValue(input: HTMLInputElement) {
    if (input) {
      input.value = '';
    }
  }

  private addTag(tag: string) {
    if (!tag || this.selected.includes(tag)) {
      return;
    }

    const tags = [...this.selected, tag];

    this.selected = tags;
    this.tagsChanged.emit(tags);
    this.control.setValue('');
    this.resetInputValue(this.tagsInput.nativeElement);
    this.openAutocomplete();
  }

  private openAutocomplete() {
    setTimeout(() => {
      this.inputAutoComplete.openPanel();
    });
  }

  private listenTagsSearch() {
    this.control.valueChanges.pipe(
      debounceTime(300),
      takeUntil(this.destroy$),
    ).subscribe(query => {
      this.searchTags.emit(query);
    });
  }
}
