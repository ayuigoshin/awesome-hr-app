import {NgModule} from '@angular/core';

import {EmployeeSearchComponent} from './employeeSearch.component';
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {TagsAutocompleteModule} from '../tagsAutocomplete/tagsAutocomplete.module';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatInputModule,
    TagsAutocompleteModule,
    MatIconModule,
    MatButtonModule,
    MatTooltipModule,
  ],
  exports: [EmployeeSearchComponent],
  declarations: [EmployeeSearchComponent],
  providers: [],
})
export class EmployeeSearchModule {
}
