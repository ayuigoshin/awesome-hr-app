import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input, OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatChipInputEvent} from '@angular/material/chips';
import {ENTER, SPACE} from '@angular/cdk/keycodes';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {Subject} from 'rxjs';
import {debounceTime, takeUntil} from 'rxjs/operators';
import {MatDialog, MatDialogModule} from '@angular/material/dialog';
import {ConfirmationComponent} from '../confirmation/confirmation.component';

@Component({
  selector: 'employee-search',
  styleUrls: ['employeeSearch.style.less'],
  templateUrl: 'employeeSearch.template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmployeeSearchComponent implements OnInit, OnDestroy {
  @Input()
  allTags: string[] = [];
  @Input()
  tags: string[] = [];

  @ViewChild('tagsInput') tagsInput: ElementRef<HTMLInputElement>;

  readonly searchControl = new FormControl('');

  readonly destroy$ = new Subject();

  @Output() search = new EventEmitter<string>();
  @Output() tagRemove = new EventEmitter<string>();
  @Output() tagsChanged = new EventEmitter<string[]>();
  @Output() manage = new EventEmitter();
  @Output() searchTags = new EventEmitter<string>();

  constructor(private readonly dialog: MatDialog) {
  }

  ngOnInit() {
    this.listenSearch();
  }

  manageTags() {
    this.manage.emit();
  }

  clearSearch() {
    this.searchControl.setValue('');
  }

  onSearchTags(query: string) {
    this.searchTags.emit(query);
  }

  onTagRemove(tag: string) {
    this.tagRemove.emit(tag);
  }

  onTagsChange(tags: string[]) {
    this.tagsChanged.emit(tags);
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  private listenSearch() {
    this.searchControl.valueChanges.pipe(
      takeUntil(this.destroy$),
      debounceTime(300),
    ).subscribe(query => {
      this.search.emit(query);
    });
  }
}
