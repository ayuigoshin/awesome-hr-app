import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'loader',
  templateUrl: './loader.template.html',
  styleUrls: ['./loader.style.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoaderComponent {
}
