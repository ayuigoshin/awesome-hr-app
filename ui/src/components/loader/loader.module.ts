import { NgModule } from '@angular/core';
import { LoaderComponent } from './loader.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

@NgModule({
  imports: [
    MatProgressSpinnerModule,
  ],
  exports: [LoaderComponent],
  declarations: [LoaderComponent],
  providers: [],
})
export class LoaderModule { }
