import { NgModule } from '@angular/core';
import { AvatarComponent } from './avatar.component';
import {MatToolbarModule} from '@angular/material/toolbar';

@NgModule({
  imports: [
    MatToolbarModule,
  ],
  exports: [AvatarComponent],
  declarations: [AvatarComponent],
  providers: [],
})
export class AvatarModule { }
