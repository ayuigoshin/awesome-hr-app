import {ChangeDetectionStrategy, Component, HostBinding, Input} from '@angular/core';
import {Pure} from '../../decorators/pure.decorator';

@Component({
  selector: 'avatar',
  templateUrl: './avatar.template.html',
  styleUrls: ['./avatar.style.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AvatarComponent {
  @Input()
  name = '';
  @Input()
  @HostBinding('style.background-color')
  color = '#fff';

  @Pure()
  get text(): string {
    if (!this.name) {
      return '';
    }

    return this.name
      .split(' ', 2)
      .map(name => name.length ? name[0].toUpperCase() : '')
      .join('');
  }
}
