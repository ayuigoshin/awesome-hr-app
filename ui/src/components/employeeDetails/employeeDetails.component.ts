import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Subject} from 'rxjs';
import {IEmployee} from '../../model/employee';
import {debounceTime, takeUntil} from 'rxjs/operators';
import {isNil} from '../../util/utils';
import {MatInput} from '@angular/material/input';
import {PhoneMaskService} from '../../services/phoneMask.service';

@Component({
  selector: 'employee-details',
  styleUrls: ['employeeDetails.style.less'],
  templateUrl: 'employeeDetails.template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmployeeDetailsComponent implements OnInit, OnDestroy {
  @Input() allTags: string[] = [];

  @Input() set employee(employee: Partial<IEmployee>) {
    if (!employee) {
      this.form.reset();
      this.isEmpty = true;

      return;
    }

    if (employee.id !== this.form.value.id) {
      this.scheduleNameInputFocus();
    }

    this.setValue(employee);
    this.isEmpty = false;
  }

  @Output() employeeChanged = new EventEmitter<any>();
  @Output() searchTags = new EventEmitter<string>();
  @Output() tagRemove = new EventEmitter<string>();

  @ViewChild('nameInput', {read: MatInput}) nameInput: MatInput;

  readonly form = new FormGroup({
    id: new FormControl(null),
    name: new FormControl(''),
    location: new FormControl(''),
    birthDate: new FormControl(''),
    phone: new FormControl(''),
    tags: new FormControl([]),
  }, this.formValidator);

  isEmpty = true;

  readonly mask = (rawValue: string) => this.phoneMaskService.getPhoneMask(rawValue);

  private readonly destroy$ = new Subject();

  constructor(private readonly phoneMaskService: PhoneMaskService) {
  }

  get employee(): Partial<IEmployee> {
    return this.form.value;
  }

  ngOnInit() {
    this.listenFormChange();
  }

  onSearchTags(query: string) {
    this.searchTags.emit(query);
  }

  onTagRemove(tag: string) {
    this.tagRemove.emit(tag);
  }

  onTagsChange(tags: string[]) {
    this.form.patchValue({
      tags,
    });
  }

  clearLocation(event: MouseEvent) {
    event.stopPropagation();
    this.form.patchValue({
      location: null,
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  private  scheduleNameInputFocus() {
    setTimeout(() => {
      this.nameInput.focus();
    });
  }

  private setValue(employee: Partial<IEmployee>) {
    const {id, name, tags, location, birthDate, phone} = employee;

    this.form.setValue(
      {
        id: id || null,
        name: name || null,
        tags: tags || [],
        location: location || null,
        birthDate: birthDate || null,
        phone: phone || null,
      },
      {emitEvent: false},
    );
  }

  private formValidator(form: FormGroup) {
    if (!isNil(form.value.id)) {
      return null;
    }

    const {name, phone} = form.value;

    if (name || phone) {
      return null;
    }

    return {
      newEmployeeError: 'Name or phone is required for new employee',
    };
  }

  private listenFormChange() {
    this.form.valueChanges.pipe(
      debounceTime(300),
      takeUntil(this.destroy$),
    ).subscribe(() => {
      if (this.form.valid) {
        this.employeeChanged.emit(this.form.value);
      }
    });
  }
}
