import {NgModule} from '@angular/core';

import {EmployeeDetailsComponent} from './employeeDetails.component';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {AvatarModule} from '../avatar/avatar.module';
import {TagsAutocompleteModule} from '../tagsAutocomplete/tagsAutocomplete.module';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatButtonModule} from '@angular/material/button';
import {MapperPipeModule} from '../../pipes/mapper/mapper.pipe.module';
import { TextMaskModule } from 'angular2-text-mask';
import {PhoneMaskService} from '../../services/phoneMask.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AvatarModule,
    MatInputModule,
    MatIconModule,
    TagsAutocompleteModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MapperPipeModule,
    TextMaskModule,
  ],
  exports: [EmployeeDetailsComponent],
  declarations: [EmployeeDetailsComponent],
  providers: [MatDatepickerModule, PhoneMaskService],
})
export class EmployeeDetailsModule {
}
