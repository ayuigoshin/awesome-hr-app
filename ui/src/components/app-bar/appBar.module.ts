import { NgModule } from '@angular/core';
import { AppBarComponent } from './appBar.component';
import {MatToolbarModule} from '@angular/material/toolbar';

@NgModule({
  imports: [
    MatToolbarModule,
  ],
  exports: [AppBarComponent],
  declarations: [AppBarComponent],
  providers: [],
})
export class AppBarModule { }
