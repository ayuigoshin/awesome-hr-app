import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'app-bar',
  templateUrl: './appBar.template.html',
  styleUrls: ['./appBar.style.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppBarComponent {
}
