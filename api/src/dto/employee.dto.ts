import { Employee } from '../entity/Employee';

export interface IEmployeeCreateDto {
  name: string;
  birthDate: string;
  phone: string;
  location: string;
  tags: string[];
}

export class EmployeeDto implements IEmployeeCreateDto {
  id: number;
  name: string;
  birthDate: string;
  phone: string;
  location: string;
  tags: string[] = [];

  static fromEmployee(entity: Employee): EmployeeDto {
    const dto = new EmployeeDto();

    dto.id = entity.id;
    dto.name = entity.name;
    dto.location = entity.location;
    dto.phone = entity.phone;
    dto.tags = entity.tags;
    dto.birthDate = entity.birthDate ? entity.birthDate.toISOString() : null;

    return dto;
  }

  static fromEmployees(entities: Employee[]): EmployeeDto[] {
    return entities.map(EmployeeDto.fromEmployee);
  }

  static toEntity(dto: EmployeeDto | Partial<EmployeeDto>): Employee {
    const entity = new Employee();

    entity.id = dto.id;
    entity.name = dto.name;
    entity.birthDate = dto.birthDate ? new Date(dto.birthDate) : null;
    entity.location = dto.location;
    entity.phone = dto.phone;
    entity.tags = dto.tags ? dto.tags : [];

    return entity;
  }
}
