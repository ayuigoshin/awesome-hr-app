import { Tag } from '../entity/Tag';

export abstract class TagDto extends String {
  static fromTag(entity: Tag): TagDto {
    return entity.name;
  }

  static fromTags(entities: Tag[]): TagDto[] {
    return entities.map(TagDto.fromTag);
  }

  static toEntity(tag: TagDto): Tag {
    return new Tag(tag as string);
  }

  static toEntities(tags: TagDto[]): Tag[] {
    return tags.map(tag => TagDto.toEntity(tag));
  }
}
