export class PaginationResultDto<T> {
  constructor(readonly items: T[], readonly totalCount = items.length) {}
}
