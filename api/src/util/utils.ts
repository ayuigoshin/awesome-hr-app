export function isNil(value: any): boolean {
  return value === undefined || value === null;
}

export function isDate(value: any): value is Date {
  return value instanceof Date;
}

export function envOrDefault(env: string, defaultValue: string) {
  return process.env[env] ? process.env[env] : defaultValue;
}
