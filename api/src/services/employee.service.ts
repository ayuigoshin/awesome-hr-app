import { Injectable } from '@nestjs/common';
import { Employee } from '../entity/Employee';
import { EntityManager, getManager, getRepository, Repository } from 'typeorm';
import { isNil } from '../util/utils';
import { EmployeeDto, IEmployeeCreateDto } from '../dto/employee.dto';
import { Order, OrderBy } from '../model/order';

@Injectable()
export class EmployeeService {

  private get repository(): Repository<Employee> {
    return getRepository(Employee);
  }

  private get manager(): EntityManager {
    return getManager();
  }

  async find(query?: string,
             tags?: string[],
             offset = 0,
             limit = 20,
             orderBy?: OrderBy<Employee>,
             order: Order = 'DESC'): Promise<EmployeeDto[]> {

    const cb = this.repository.createQueryBuilder();
    let select = cb.select().offset(offset).limit(limit);

    if (!isNil(query)) {
      select = select.andWhere('lower(name) ~ lower(:query)', {
        query,
      });
    }

    if (!isNil(orderBy)) {
      select = select.orderBy(orderBy, order);
    }

    if (!isNil(tags)) {
      select = select.andWhere('tags ?& :tags', {
        tags,
      });
    }

    const result = await select.getMany();

    return EmployeeDto.fromEmployees(result);
  }

  async findById(id: number): Promise<EmployeeDto> {
    const employee = await this.repository.findOne({ id });

    return EmployeeDto.fromEmployee(employee);
  }

  async create(employee: IEmployeeCreateDto): Promise<EmployeeDto> {
    const entity = await this.repository.save(EmployeeDto.toEntity(employee));

    return EmployeeDto.fromEmployee(entity);
  }


  async update(id: number, employee: EmployeeDto): Promise<EmployeeDto> {
    employee.id = Number(id);

    await this.repository.update(id, EmployeeDto.toEntity(employee));

    return employee;
  }

  async patch(id: number, employee: Partial<EmployeeDto>): Promise<EmployeeDto> {
    const entity = await this.manager.transaction(async entityManager => {
      const repository = entityManager.getRepository(Employee);

      await repository.update({ id }, EmployeeDto.toEntity(employee));

      return repository.findOne({ id });
    });

    return EmployeeDto.fromEmployee(entity);
  }

  async delete(id: number): Promise<number> {
    await this.repository.delete({ id });

    return id;
  }

  count(query?: string, tags?: string[]) {
    const cb = this.repository.createQueryBuilder();
    let select = cb.select();

    if (!isNil(query)) {
      select = select.andWhere('lower(name) ~ lower(:query)', {
        query,
      });
    }

    if (!isNil(tags)) {
      select = select.andWhere('tags ?& :tags', {
        tags,
      });
    }

    return select.getCount();
  }

  async removeTag(tag: string) {
    await this.repository.query('UPDATE employee SET tags = tags - $1', [tag]);
  }
}
