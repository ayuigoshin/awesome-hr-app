import { Injectable } from '@nestjs/common';
import { getRepository, Repository } from 'typeorm';
import { Tag } from '../entity/Tag';
import { TagDto } from '../dto/tag.dto';
import { isNil } from '../util/utils';
import { of } from 'rxjs';

@Injectable()
export class TagService {

  private get repository(): Repository<Tag> {
    return getRepository(Tag);
  }

  async find(query?: string, offset?: number, limit?: number): Promise<TagDto[]> {
    const cb = this.repository.createQueryBuilder();
    let select = cb.select().orderBy('name');

    if (!isNil(offset)) {
      select = select.offset(offset);
    }

    if (!isNil(limit)) {
      select = select.limit(limit);
    }

    if (!isNil(query) && query !== '') {
      select = select.where('lower(name) ~ lower(:query)', {
        query,
      });
    }

    const entities = await select.getMany();

    return TagDto.fromTags(entities);
  }

  count(query?: string): Promise<number> {
    const cb = this.repository.createQueryBuilder();
    let select = cb.select();

    if (!isNil(query) && query !== '') {
      select = select.where('lower(name) ~ lower(:query)', {
        query,
      });
    }

    return select.getCount();
  }

  async createIfAbsent(tag: string): Promise<void> {
    await this.addAbsent([tag])
  }

  async addAbsent(tags: string[]) {
    if (!tags.length) {
      return;
    }

    const cb = this.repository.createQueryBuilder();
    const preparedTags = tags.map(tag => tag.trim())

    await cb.insert().values(TagDto.toEntities(preparedTags))
      .onConflict(`("name") DO NOTHING`)
      .execute();
  }

  async remove(tag: string): Promise<string> {
    await this.repository.delete({name: tag});

    return tag;
  }
}
