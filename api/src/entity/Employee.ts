import {Entity, PrimaryGeneratedColumn, Column} from 'typeorm';

@Entity({name: 'employee'})
export class Employee {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: true})
    name: string;

    // requires lowercase names for sorting
    @Column({name: 'birthdate', nullable: true, type: 'timestamp'})
    birthDate: Date;

    @Column({nullable: true})
    phone: string;

    @Column({nullable: true})
    location: string;

    @Column({ type: 'jsonb'})
    tags: string[];
}
