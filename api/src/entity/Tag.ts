import { Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class Tag {
  @PrimaryColumn({ unique: true })
  name: string;

  constructor(name: string) {
    this.name = name;
  }

}
