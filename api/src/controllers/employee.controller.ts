import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Patch,
  Post,
  Put,
  Query,
  Response,
  UseFilters,
  BadRequestException,
} from '@nestjs/common';
import { Employee } from '../entity/Employee';
import { EmployeeService } from '../services/employee.service';
import { Order, OrderBy } from '../model/order';
import { EmployeeDto, IEmployeeCreateDto } from '../dto/employee.dto';
import { PaginationResultDto } from '../dto/pagination.dto';
import { TagService } from '../services/tag.service';
import { BadRequestExceptionFilter } from '../filters/badRequest.filter';

@Controller('api/v1/employee')
export class EmployeeController {
  constructor(private readonly employeeService: EmployeeService,
              private readonly tagService: TagService) {
  }

  @Get()
  async getAll(@Query('query') query?: string,
               @Query('tags') tags?: string,
               @Query('limit') limit?: number,
               @Query('offset') offset?: number,
               @Query('orderBy') orderBy?: OrderBy<Employee>,
               @Query('order') order?: Order): Promise<PaginationResultDto<EmployeeDto>> {
    const tagsArr = tags ? tags.split(',') : null;
    const employees = await this.employeeService.find(query, tagsArr, offset, limit, orderBy, order);
    const totalCount = await this.employeeService.count(query, tagsArr);

    return new PaginationResultDto(employees, totalCount);
  }

  @Get(':id')
  get(@Param('id') id: number): Promise<EmployeeDto> {
    return this.employeeService.findById(id);
  }

  @Post()
  @UseFilters(new BadRequestExceptionFilter())
  async create(@Body() employee: IEmployeeCreateDto): Promise<EmployeeDto> {
    if (!employee.name && !employee.phone) {
      throw new BadRequestException('New employee must contain phone or name!', 'ValidationException')
    }

    await this.tagService.addAbsent(employee.tags);

    return await this.employeeService.create(employee);;
  }

  @Put(':id')
  async update(@Param('id') id: number, @Body() employee: EmployeeDto): Promise<EmployeeDto> {
    await this.tagService.addAbsent(employee.tags);

    return this.employeeService.update(id, employee);
  }

  @Patch(':id')
  async patch(@Param('id') id: number, @Body() employee: Partial<EmployeeDto>): Promise<EmployeeDto> {
    await this.tagService.addAbsent(employee.tags);

    return this.employeeService.patch(id, employee);
  }

  @Delete(':id')
  async delete(@Param('id') id: number): Promise<{ id: number }> {
    await this.employeeService.delete(id);

    return { id };
  }
}
