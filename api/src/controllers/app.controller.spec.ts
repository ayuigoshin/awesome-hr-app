import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { EmployeeService } from '../services/employee.service';
import { getRepository } from 'typeorm';
import { Employee } from '../entity/Employee';
import { from } from 'rxjs';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [EmployeeService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });
});
