import { Controller, Get, HttpCode } from '@nestjs/common';

@Controller()
export class AppController {
  constructor() {}

  @Get('/hc')
  @HttpCode(200)
  healthCheck(): string {
    return 'OK!'
  }
}
