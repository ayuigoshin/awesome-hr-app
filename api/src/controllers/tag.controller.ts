import { Body, Controller, Delete, Get, HttpCode, Param, Post, Query } from '@nestjs/common';
import { TagService } from '../services/tag.service';
import { TagDto } from '../dto/tag.dto';
import { PaginationResultDto } from '../dto/pagination.dto';
import { EmployeeService } from '../services/employee.service';
import { EmployeeDto } from '../dto/employee.dto';

@Controller('api/v1/tag')
export class TagController {
  constructor(private readonly tagService: TagService,
              private readonly employeeService: EmployeeService) {
  }

  @Get()
  async getAll(@Query('query') query?: string,
               @Query('offset') offset?: number,
               @Query('limit') limit?: number): Promise<PaginationResultDto<TagDto>> {
    const tags = await this.tagService.find(query, limit, offset);
    const totalCount = await this.tagService.count(query);

    return new PaginationResultDto(tags, totalCount);
  }

  @Post()
  async create(@Body() tags: string[]) {
    await this.tagService.addAbsent(tags);
  }

  @Delete(':tag')
  async delete(@Param('tag') tag: string): Promise<string[]> {
    await this.employeeService.removeTag(tag);
    await this.tagService.remove(tag);

    return [tag];
  }
}
