import { Module } from '@nestjs/common';
import { AppController } from './controllers/app.controller';
import { TagController } from './controllers/tag.controller';
import { EmployeeService } from './services/employee.service';
import { TagService } from './services/tag.service';
import { EmployeeController } from './controllers/employee.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import {envOrDefault} from './util/utils';
import { Employee } from './entity/Employee';
import { Tag } from './entity/Tag';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: envOrDefault('TYPEORM_HOST', 'localhost'),
      port: 5432,
      username: 'postgres',
      password: 'postgres',
      database: envOrDefault('TYPEORM_DATABASE', 'hr-app'),
      entities: [Employee, Tag],
      synchronize: true,
    }),
  ],
  controllers: [AppController, EmployeeController, TagController],
  providers: [EmployeeService, TagService],
})
export class AppModule { }
